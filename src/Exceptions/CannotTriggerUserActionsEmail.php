<?php

namespace Scito\Keycloak\Admin\Exceptions;

use RuntimeException;

class CannotTriggerUserActionsEmail extends RuntimeException
{

}
