<?php

namespace Scito\Keycloak\Admin\Resources;

use Scito\Keycloak\Admin\Representations\UserRepresentationInterface;

interface UserResourceInterface
{
    public function toRepresentation(): UserRepresentationInterface;

    public function roles(): UserRolesResourceInterface;

    public function update(?array $options = null): UserUpdateResourceInterface;

    /**
     * Trigger sending an Actions Email
     *
     * Parameter documentation see
     * https://www.keycloak.org/docs-api/11.0/rest-api/index.html#_executeactionsemail
     * @param array associate array with optional parameters to the query URL
     *
     * The user must be enabled for this to work.
     */
    public function sendActionsEmail(
        array $actions,
        ?array $optionalParameters = null
    );

    public function getRealm(): string;

    public function getId(): string;

    public function delete();
}
